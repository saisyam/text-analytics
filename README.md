# Text Analytics
Text Analytics using Python tools.

# Setting up Environment
It is preferrable to setup a virtual environment for Python projects. Below are the list of packages required to run this code to be installed inside the virtual environment.
```
$ pip3 install spacy numpy
$ python -m spacy download en_core_web_sm
$ pip3 install spacy-lookups-data
```

# Text Filtering
Text filtering is the first step we do in text analytics to clean the text. Filtering process of text involves:

1. Removing punctuation, newlines etc
2. Removing [stopwords](https://kavita-ganesan.com/what-are-stop-words)
3. [Lemmentizing](https://nlp.stanford.edu/IR-book/html/htmledition/stemming-and-lemmatization-1.html) the words

[filtertext.py](https://gitlab.com/saisyam/text-analytics/blob/master/filtertext.py) - sample code to do text filtering
import re
import spacy
import string

class FilterText:
    
    def __init__(self):
        self.nlp = spacy.load('en_core_web_sm')
    
    def clean_text(self, text):
        text = text.lower()
        text = re.sub(r"what's", "what is ", text)
        text = re.sub(r"\'s", " ", text)
        text = re.sub(r"\'ve", " have ", text)
        text = re.sub(r"can't", "can not ", text)
        text = re.sub(r"n't", " not ", text)
        text = re.sub(r"i'm", "i am ", text)
        text = re.sub(r"\'re", " are ", text)
        text = re.sub(r"\'d", " would ", text)
        text = re.sub(r"\'ll", " will ", text)
        text = re.sub(r"\'scuse", " excuse ", text)
        text = re.sub('\W', ' ', text)
        text = re.sub('\s+', ' ', text)
        text = text.strip(' ')
        return text
    
    def remove_stop_words(self, text):
        txt = self.clean_text(text)
        doc = self.nlp(txt)
        words = [token.text for token in doc if token.is_stop != True and token.is_punct != True]
        return words

    def lemmatize(self, text):
        txt = self.clean_text(text)
        doc = self.nlp(txt)
        lemma = [token.lemma_ for token in doc if token.is_stop != True and token.is_punct != True]
        return lemma 

ct = FilterText()
text = """GREAT food and cool atmosphere. Went here for brunch today with dear friend and we had the grapefruit 
with ginger sugar somethin, yogurt (great combo of rubharb, honey and pistachio, and baked egg with chorizo and tomato. 
All were excellent. Service was solid too, not very friendly, but nice enough. The portions are small."""

print(ct.remove_stop_words(text))
print(ct.lemmatize(text))

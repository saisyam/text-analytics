# Categorizing restaurant reviews

The zip file, [food_review_data.zip](https://drive.google.com/file/d/1FuKauKoeka_GBQ1pc9ALIp59RqZnS8TC/view?usp=sharing) contains following files:

1. `food_business.json` - contains filtered list of food related businesses
2. `food_reviews.json` - contains reviews related to food businesses
3. `review_cat.json` - categoized reviews using `processreviews.py` 

`review_cat.json` contains multi-labelled reviews. The labels used are, `food`, `price`, `ambience` and `service`.
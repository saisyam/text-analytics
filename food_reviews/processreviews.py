from spacy.lang.en import English
from spacy.lang.en.stop_words import STOP_WORDS
import json
import spacy

nlp = spacy.load('en_core_web_sm')

keywords = ["food", "service", "staff", "price", "expensive", "place", "ambience"]
def remove_stopwords(text):
    my_doc = nlp(text)
    
    # Create list of word tokens
    token_list = []
    for token in my_doc:
        token_list.append(token.text)
    filtered_sentence =[] 
    for word in token_list:
        lexeme = nlp.vocab[word]
        if lexeme.is_stop == False:
            filtered_sentence.append(word)
    return ' '.join(filtered_sentence).lower()

def get_category(word):
    if word == "expensive":
        return "price"
    elif word == "place":
        return "ambience"
    elif word == "staff":
        return "service"
    else:
        return word

def get_noun_chunks(text):
    doc = nlp(text)
    nchunks = []
    category = []
    for chunk in doc.noun_chunks:
        for i in keywords:
            if i in chunk.text:
                clean_text = remove_stopwords(chunk.text)
                if clean_text not in nchunks:
                    nchunks.append(clean_text)
                if get_category(i) not in category:
                    category.append(get_category(i))
    return nchunks, category

fp = open('review_cat.json', 'w')
with open('food_reviews.json', 'r') as f:
    for line in f:
        nobj = {}
        jobj = json.loads(line)
        text = jobj['text']
        noun_chunks, category = get_noun_chunks(text)
        if len(category) != 0:
            nobj['text'] = jobj['text']
            nobj['stars'] = jobj['stars']
            nobj['keywords'] = noun_chunks
            nobj['food'] = 0
            nobj['service'] = 0
            nobj['price'] = 0
            nobj['ambience'] = 0
            for cat in category:
                if cat == 'price':
                    nobj['price'] = 1
                elif cat == 'service':
                    nobj['service'] = 1
                elif cat == 'food':
                    nobj['food'] = 1
                elif cat == 'ambience':
                    nobj['ambience'] = 1
            nobj['category'] = category
            fp.write(json.dumps(nobj))
            fp.write("\r\n")
    f.close()
fp.close()